XDMS
=========


This installation script install the following software components:

- xdms.2.0.0.war

Requirements
------------

System requirements:

- Glassfish v5.0
- MySQL

Role Variables
--------------

- **install_host** (Default: ptt) Host on /etc/ansible/hosts to run the script
- **type** (You have to indicate the correct value for the operation you want to do. Possible options are: Install, Upgrade and UnInstall)
- **deployment** (You have to indicate the correct value for the deployment you want to do. Possible options are: SA, HA and Geored)
- **db_IP** (It must be 127.0.0.1) The ip address to access the database as xdms user
- **pttserver_IP**  The traffic ip address of MCPTT Server
- **mysql_xdms_user** (Default: xdms) DB user (rw) that will by used by xdms web ap
- **mysql_xdms_password** (Default: xdms123) DB Password that will set for xdms user
- **xdms_logfile** (Default: /var/log/pocserverplatform/xdms.log) Path to store xdms logs
- **glassfish_admin_pwd** (Default: admin123) Glassfish Password for admin user
- **xdms_version** (Default: 1.8.2) The xdms version

Firewall Rules
--------------

IN/OUT | PROTOCOL | FROM IP | TO IP | FROM PORT | TO PORT 
-------|----------|---------|-------|-----------|--------
inbound | TCP | ANY | <IP> | ANY | 8888

Logs
----

Xdms logs are written in `/var/log/pocserverplatform/xdms.log` to change the path manually `/opt/glassfish5/glassfish/domains/domain1/applications/xdms-1.8.1/WEB-INF/classes/log4j.xml` shold be edited

Dependencies
------------

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

Use the following commands to execute the script (Set the variables properly)

## For IPv4
	X_PTT=10.1.1.2
	ansible-playbook -e "pttserver_IP=$X_PTT" xdms.yml

## For IPv6
	X_PTT=2001:470:1f21:219:20c:29ff:j5h3:12sa
	ansible-playbook -e "pttserver_IP=$X_PTT" xdms.yml

Author Information
------------------

Genaker
http://www.genaker.net
