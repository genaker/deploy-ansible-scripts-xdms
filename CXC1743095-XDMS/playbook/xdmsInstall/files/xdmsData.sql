use xdms;

LOCK TABLES `dmz` WRITE;
/*!40000 ALTER TABLE `dmz` DISABLE KEYS */;
INSERT INTO `dmz` VALUES (1,0,'*','any');
/*!40000 ALTER TABLE `dmz` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `monitoring` WRITE;
/*!40000 ALTER TABLE `monitoring` DISABLE KEYS */;
INSERT INTO `monitoring` VALUES (1,'path','/var/log/pocserverplatforma'),(2,'interval','-1'),(3,'type','xml'),(4,'serverId','xdms');
/*!40000 ALTER TABLE `monitoring` ENABLE KEYS */;
UNLOCK TABLES;

